<?php

class pluginTelegrammSend extends Plugin {

	public function init() 	{ 		
    $this->dbFields = array(
        'botToken'=>'', 
        'chat2post'=>''
    ); 
	}

	public function form()
	{
		global $Language;

		$html = '<div>'; 		
		$html .= '<label>'.$Language->get('lbl token').'</label>'; 		
		$html .= '<input name="botToken" id="botToken" type="text" value="'.$this->getValue('botToken').'">'; 
		$html .= '</div>';

		$html .= '<div>'; 		
		$html .= '<label>'.$Language->get('lbl chatid').'</label>'; 		
		$html .= '<input name="chat2post" id="chat2post" type="text" value="'.$this->getValue('chat2post').'">';	
		$html .= '</div>';

		$html .= '<div>'; 		
		$html .= '<a target="_blank" href="https://api.telegram.org/bot'. $this->getValue('botToken').'/getUpdates">'.$Language->get('lbl link').'</a>'; 		
		$html .= '</div>';
		return $html;
	}


	public function afterPageCreate()
	{
		global $dbPages;

		$pageNumber = 1; 		
		$amountOfItems = 1; 		
		$onlyPublished = true; 		
		$pages = $dbPages->getList($pageNumber, $amountOfItems, $onlyPublished); 		
		foreach($pages as $pageKey) {		
			$page = buildPage($pageKey); 	
		}

		$txt = $page->permalink();

		$sendToTelegram = fopen("https://api.telegram.org/bot{$this->getValue('botToken')}/sendMessage?chat_id={$this->getValue('chat2post')}&parse_mode=html&text={$txt}","r");
	}

}